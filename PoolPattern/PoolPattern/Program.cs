﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PoolPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            MyConnectionPool.Instance.GetConnection(); // blocking
            // Task:
            // ...Blocking
            // ...Sync
            // ...Async

            // ................
            
        }
    }
}
