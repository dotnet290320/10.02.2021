﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace File_Sol
{
    class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(() => MyFileManager.Instance.WriteToFile("Line1"));
            Thread t2 = new Thread(() => MyFileManager.Instance.WriteToFile("Line2"));
            Thread t3 = new Thread(() => MyFileManager.Instance.WriteToFile("Line3"));
            Thread t4 = new Thread(() => MyFileManager.Instance.DeleteFile());
            Thread t5 = new Thread(() => MyFileManager.Instance.CreateFile());
            Thread t6 = new Thread(() => MyFileManager.Instance.WriteToFile("Line4"));
            Thread t7 = new Thread(() => MyFileManager.Instance.CreateFile());
            //Thread t8 = new Thread(() => MyFileManager.Instance.DeleteFile());
            Thread t8 = new Thread(() => MyFileManager.Instance.WriteToFile("Done."));
            Thread t9 = new Thread(() => MyFileManager.Instance.WriteToFile("Line5"));

            t1.Start();
            Thread.Sleep(1000);
            t2.Start();
            Thread.Sleep(1000);
            t3.Start();
            Thread.Sleep(1000);
            t4.Start();
            Thread.Sleep(1000);
            t5.Start();
            Thread.Sleep(1000);
            t6.Start();
            Thread.Sleep(1000);
            t7.Start();
            Thread.Sleep(1000);
            t8.Start();
            Thread.Sleep(1000);
            t9.Start();
            Thread.Sleep(1000);
        }
    }
}
